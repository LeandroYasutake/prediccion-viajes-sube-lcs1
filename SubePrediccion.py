# -*- coding: utf-8 -*-
"""
Created on Wed Aug 31 19:39:39 2022

@author: Leo
"""

#deshabilito alertas
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import LSTM,Dense
from sklearn.preprocessing import MinMaxScaler

sube_data = pd.read_csv("SUBEferrocarril.csv")

#descartamos columnas innecesarias
sube_data = sube_data.drop(columns = ['NOMBRE_EMPRESA','LINEA','AMBA','TIPO_TRANSPORTE'
                                      ,'JURISDICCION','PROVINCIA', 'MUNICIPIO','DATO_PRELIMINAR'],axis=1)

#convertimos DIA_TRANSPORTE en tipo date
sube_data["DIA_TRANSPORTE"] = pd.to_datetime(sube_data.DIA_TRANSPORTE, format="%Y/%m/%d")

#ordenamos datos por fecha y cantidad de viajes por dia
sube_data.index = sube_data["DIA_TRANSPORTE"]
sube_data = sube_data.groupby(sube_data.index).agg({'CANTIDAD': 'sum'}).reset_index()

"""separamos los datos necesarios, la cantidad de viajes y para el entrenamiento
nos quedamos con el 80%"""
cantidad_viajes = sube_data['CANTIDAD']
valores_viajes = cantidad_viajes.values
porcentaje_entrenamiento = 0.8
lenght_data_entrenamiento = math.ceil(len(valores_viajes)* porcentaje_entrenamiento)

"""se utiliza MinMaxScaler para normalizar todos nuestros datos de 0 a 1 
también remodelamos nuestros datos normalizados en una matriz 2D"""
scaler = MinMaxScaler(feature_range=(0,1))
data_escalada = scaler.fit_transform(valores_viajes.reshape(-1,1))
data_entrenamiento = data_escalada[0: lenght_data_entrenamiento, :]

x_train,y_train=[],[]

"""usamos ventana de 60 días de viajes históricos (i-60) como nuestros 
datos independientes (x_train) y la siguiente ventana de 60 días como 
datos dependientes (y_train)"""
for i in range(60, len(data_entrenamiento)):
    x_train.append(data_entrenamiento[i-60:i, 0])
    y_train.append(data_entrenamiento[i, 0])

"""convertimos los datos (x_train) y (y_train) en una matriz Numpy, ya que es el formato
de datos aceptado por Tensorflow Keras cuando entrena un modelo de red neuronal"""
x_train, y_train = np.array(x_train), np.array(y_train)
x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))

"""preparamos datos de testeo
extraemos los viajes de nuestro conjunto de datos normalizados 
(el último 20% del conjunto de datos)"""
data_test = data_escalada[lenght_data_entrenamiento-60: , : ]
x_test = []
y_test = valores_viajes[lenght_data_entrenamiento:]

for i in range(60, len(data_test)):
  x_test.append(data_test[i-60:i, 0])


x_test = np.array(x_test)
x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

"""se define un modelo secuencial que consta de una pila lineal de capas
capa lstm de 100 unidades de red
capa lstm con también 50 unidades de red
capa densa con 1 unidades de red"""
lstm_model = Sequential()
lstm_model.add(LSTM(units=100,return_sequences=True,input_shape=(np.shape(x_train)[1],1)))
lstm_model.add(LSTM(units=50))
lstm_model.add(Dense(1))

lstm_model.compile(optimizer='adam', loss='mean_squared_error')
lstm_model.fit(x_train, y_train, batch_size=8, epochs=516)
"""evaluacion de modelo
desnormalizamos la data para averiguar el root mean square error"""
predicciones = lstm_model.predict(x_test)
predicciones = scaler.inverse_transform(predicciones)

"""
para mejorar la precision
history1 = lstm_model.fit(x_train, y_train, batch_size=8, epochs=526)
plt.xlabel("epoca")
plt.ylabel("mag de perdida")
plt.plot(history1.history["loss"])
mape = np.mean(np.abs((y_test-predicciones)/y_test)*100)
print(mape)
"""
                
data = sube_data.filter(['CANTIDAD'])
data.index=sube_data["DIA_TRANSPORTE"]
train = data[:lenght_data_entrenamiento]
data_test = data[lenght_data_entrenamiento:]
data_test['predicciones'] = predicciones

plt.figure(figsize=(16,8))
plt.title('Modelo LTSM: Azul=Viajes hechos - Rojo=Predicciones Verde=Viajes 18/08 al 31 - Cyan=Predicciones a futuro')
plt.xlabel('DIA_TRANSPORTE')
plt.ylabel('VIAJES')
plt.ticklabel_format(style='plain')
plt.scatter(sube_data["DIA_TRANSPORTE"],sube_data["CANTIDAD"],color = 'blue')
plt.scatter(data_test.index, data_test['predicciones'],color = 'red')


##########################################################################


dias = 0
while not int(dias) in range(1,121):
    dias = int(input("Ingresar cantidad a futuro de días a predecir (1 - 120) : "))

"""ahora predecimos X dias a futuro y reentrenamos con una ventana
pasada de 60 dias"""
n_dias_atras = 60  # ventana de dias pasados a mirar
n_dias_prediccion = dias  # ventana de dias a futuro

x_train_pred , y_train_pred = [] , []

for i in range(n_dias_atras, len(data_entrenamiento) - n_dias_prediccion + 1):
    x_train_pred.append(data_entrenamiento[i - n_dias_atras: i])
    y_train_pred.append(data_entrenamiento[i: i + n_dias_prediccion])

x_train_pred, y_train_pred = np.array(x_train_pred) , np.array(y_train_pred)

#re entrenamos modelo
modelo_futuro = Sequential()
modelo_futuro.add(LSTM(units=100, return_sequences=True, input_shape=(n_dias_atras, 1)))
modelo_futuro.add(LSTM(units=50))
modelo_futuro.add(Dense(n_dias_prediccion))
modelo_futuro.compile(loss='mean_squared_error', optimizer='adam')
modelo_futuro.fit(x_train_pred, y_train_pred, epochs=1000, batch_size=8)

#generamos la prevision
x_prev = data_entrenamiento[- n_dias_atras:]  #ultima entrada de la secuencia
x_prev = x_prev.reshape(1, n_dias_atras, 1)

#generamos la prediccion
y_prev = modelo_futuro.predict(x_prev).reshape(-1, 1)
y_prev = scaler.inverse_transform(y_prev)

#organizamos la data
sube_data_pasada = sube_data
sube_data_pasada.rename(columns={'CANTIDAD': 'Actual'}, inplace=True)
sube_data_pasada['Predicción'] = np.nan
sube_data_pasada['Predicción'].iloc[-1] = sube_data_pasada['Actual'].iloc[-1]

df_futuro = pd.DataFrame(columns=['DIA_TRANSPORTE', 'Actual', 'Predicción'])
df_futuro['DIA_TRANSPORTE'] = pd.date_range(start=sube_data_pasada['DIA_TRANSPORTE'].iloc[-1] + pd.Timedelta(days=1), periods=n_dias_prediccion)
df_futuro['Predicción'] = y_prev.flatten()
df_futuro['Actual'] = np.nan

resultados = sube_data_pasada.append(df_futuro).set_index('DIA_TRANSPORTE')

plt.scatter(resultados.index, resultados['Predicción'],color = 'green')

"""history1 = modelo_futuro.fit(x_train_pred, y_train_pred, epochs=1000, batch_size=8, verbose=0)
para mejorar la magnitud de perdida
plt.xlabel("epoca")
plt.ylabel("mag de perdida")
plt.plot(history1.history["loss"])"""

##########################################################################

"""ploteamos los nuevos 13 dias"""

restante_agosto_data = pd.read_csv("SUBE-13dias-agosto.csv")
#descartamos columnas innecesarias
restante_agosto_data = restante_agosto_data.drop(columns = ['NOMBRE_EMPRESA','LINEA','AMBA','TIPO_TRANSPORTE'
                                      ,'JURISDICCION','PROVINCIA', 'MUNICIPIO','DATO_PRELIMINAR'],axis=1)
#convertimos DIA_TRANSPORTE en tipo date
restante_agosto_data.index = restante_agosto_data["DIA_TRANSPORTE"]
restante_agosto_data["DIA_TRANSPORTE"] = pd.to_datetime(restante_agosto_data.DIA_TRANSPORTE, format="%Y/%m/%d")
restante_agosto_data = restante_agosto_data.groupby(restante_agosto_data.index).agg({'CANTIDAD': 'sum'}).reset_index()
restante_agosto_data = restante_agosto_data[(restante_agosto_data['DIA_TRANSPORTE'] > '2022-08-18') 
                      & (restante_agosto_data['DIA_TRANSPORTE'] <= '2022-08-31')]

plt.scatter(restante_agosto_data["DIA_TRANSPORTE"],restante_agosto_data["CANTIDAD"],color="cyan") 
plt.show()